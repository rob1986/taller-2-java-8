/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bce.ddi.streams;

import bce.ddi.entidad.Persona;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author Byron Bonilla
 * @author Roberto Lagos
 */
public class TallerTest {

    /**
     * @param args main
     */
    public static void main(String[] args) {

        String os = System.getProperty("os.name");
        URL urlArchivo = Thread.currentThread().getContextClassLoader().getResource("BaseJava8.txt");
        String pathArchivo = urlArchivo.getPath();
        if (os.contains("Windows")) {
            //Remueve el caracter / al inicio para ambientes Windows
            pathArchivo = urlArchivo.getPath().substring(1, urlArchivo.getPath().length());
        }

        try {

            List<Persona> lstPersonas = new ArrayList<>();
            Stream<String> streamFile = Files.lines(Paths.get(pathArchivo));

            System.out.println("Aplicacion de Tres Filtros Simultaneamente");
            System.out.println("------------------------------------------");
            System.out.println("Estado Civil Soltero");            
            System.out.println("Valor de Sueldo Valido");
            System.out.println("Sueldo Mayor a 600");
            System.out.println("------------------------------------------");
            Map<Boolean, List<String[]>> mapaProcesamiento = streamFile.parallel().map(linea -> linea.split(","))
                    .filter(m -> m[5].equalsIgnoreCase("Soltero"))
                    .filter(m -> {
                        try {
                            Double.parseDouble(m[6]);
                            return true;
                        } catch (Exception e) {
                            return false;
                        }
                    }).collect(Collectors.partitioningBy(TallerTest::validarSueldo));

            System.out.println("\nMapeo de Objetos Correctos a Traves de un Stream");
            System.out.println("------------------------------------------------");
            lstPersonas = mapaProcesamiento.get(true).stream()
                    .map(m -> {
                        return new Persona(m[0], m[1], m[2], Integer.parseInt(m[3]), m[4], m[5], Double.parseDouble(m[6]), m[7]);
                    }).collect(Collectors.toList());

            System.out.println("\nImprimiesndo Resultados Correctos");
            System.out.println("---------------------------------");
            lstPersonas.forEach(System.out::println);

            System.out.println("\nAplicando Operación Reduce");
            System.out.println("--------------------------");
            lstPersonas.stream().reduce(TallerTest::sumar).ifPresent(p -> System.out.println("Total Monto Salarios :" + p.getSalario()));

        } catch (Exception e) {
            System.out.println("Ocurrió una excepción: " + e);
        }
    }

    public static boolean validarSueldo(String[] linea) {
        if (Double.parseDouble(linea[6]) > 600) {
            return true;
        }
        return false;
    }

    public static Persona sumar(Persona n1, Persona n2) {
        n1.setSalario(n1.getSalario() + n2.getSalario());
        return n1;
    }

}
